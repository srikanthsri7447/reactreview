const fs = require('fs')
const path = require('path')


const readData=()=>{
    return new Promise((resolve,reject)=>{
        fs.readFile((path.join('funds.json'+'')) , 'utf-8', (err,data)=>{
            if(err){
                return reject(err)
            }
            else{
                return resolve(JSON.parse(data))
            }
    })
})

}

readData()
.then((data)=>{

    const resultArray = data.reduce((output,eachObj)=>{ 

        if(!output[eachObj.fund_house]){
            output[eachObj.fund_house] = 1
            return output
        }
        else{
            output[eachObj.fund_house] += 1 
            return output
        }

       
    },{})

    fs.writeFile(path.join('noOfFundsPerFund_house.txt',""),JSON.stringify(resultArray), 'utf-8',(err)=>{
        if(err){
            console.log(err.message)
        }
        else{
            console.log('Created noOfFundsPerFund_house')
        }
    })
})
.catch(console.log)
