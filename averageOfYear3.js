const fs = require('fs')
const path = require('path')


const readData = () => {
    return new Promise((resolve, reject) => {
        fs.readFile((path.join('funds.json' + '')), 'utf-8', (err, data) => {
            if (err) {
                return reject(err)
            }
            else {
                return resolve(JSON.parse(data))
            }
        })
    })

}

readData()
    .then((data) => {

        let counterObj ={}

        const resultData = data.reduce((output,eachFund) => {

            if(!eachFund.returns){
                return output
            }
            else{
                if(!eachFund.returns['year_3']){
                    return output
                }
                else{
                    if(!output[eachFund.fund_house]){

                        counterObj[eachFund.fund_house] = 1

                        output[eachFund.fund_house] = parseFloat(Number(eachFund.returns['year_3'])/Number(counterObj[eachFund.fund_house]))
                         
                        return output
                    }
                    else{
    
                        counterObj[eachFund.fund_house] += 1
    
                        output[eachFund.fund_house] = parseFloat((output[eachFund.fund_house] + Number(eachFund.returns['year_3']))/Number(counterObj[eachFund.fund_house]))
                         
                        return output
                    }
    
                }
    
            }

           
        },{})

        fs.writeFile(path.join('averageOfYear3.txt',''),JSON.stringify(resultData),'utf-8',(err)=>{
            if(err){
                console.log(err)
            }
            else{
                console.log('Create Average of year_3 Successfully')
            }
        })

    })
    .catch(console.log)
